<?php

if (!isset($antiCheat) || $antiCheat !== 1)
{
	header('location: index.php');
	exit();
}

unset($_SESSION["captcha"]);
unset($_SESSION["form"]);
unset($_SESSION["error"]);

require 'views/sended.phtml';