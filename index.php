<?php
session_start();

$errorDisplay = [0=>"", 1=>"error"];
$numbers = [0=>"zero", 1=>"un", 2=>"deux", 3=>"trois", 4=>"quarte", 5=>"cinq", 6=>"six", 7=>"sept", 8=>"huit", 9=>"neuf"];
$requiredFields = 
[
	"nom",
	"prenom",
	"mail",
	"tel",
	"captcha",
	"message"
];

$number1 = rand(1,9);
$number2 = rand(1,9);

$_SESSION["captcha"] = $number1."FA542B5C".$number2;

$display = $numbers[$number1] . " + " . $numbers[$number2];

if (isset($_SESSION["error"]) && !empty($_SESSION["error"]))
{
	$error = $_SESSION["error"];
}
else
{
	foreach ($requiredFields as $value) {
		$error[$value] = false;
	}
}

if (isset($_SESSION["form"]) && !empty($_SESSION["form"]))
{
	$fields = $_SESSION["form"];
}
else
{
	foreach ($requiredFields as $value) {
		$fields[$value] = "";
	}
}

require 'views/index.phtml';