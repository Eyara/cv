document.addEventListener('DOMContentLoaded', function() 
{
	var countdown = parseInt(document.getElementById('countdown').textContent);
	var chrono;

	var textDiv = document.querySelector("main>div:last-child");
	var texts = [];
	for (i=0; i<textDiv.childNodes.length; i++)
	{
		if (textDiv.childNodes[i].nodeType === 1)
		{
			texts.push(textDiv.childNodes[i]);
		}
	}
	var current = 0;
	window.setTimeout(write, 1000);

	function write()
	{
		texts[current].classList.remove('hidden');
		window.setTimeout( function() { texts[current].classList.add('shown'); } ,100);
		if (current < texts.length -1)
		{
			window.setTimeout( function() { 
				texts[current].classList.remove('shown');
				window.setTimeout( function() { 
					texts[current].classList.add('hidden');
					current++;
					window.setTimeout(write, 1000);
				} ,1000);
			} ,5000);
		}
		else if ( current == texts.length - 1 )
		{
			if ( countdown > 0 )
			{
				window.setTimeout( function() {
					chrono = window.setInterval(timer, 1000);
				} ,1000);
			}
		}
	}

	function timer()
	{
		countdown--;
		document.getElementById('countdown').innerHTML = countdown;
		if ( countdown <= 0 )
		{
			window.clearInterval(chrono);
			window.setTimeout( function() { window.location.href = "./"; }, 1000);
		}
	}
});