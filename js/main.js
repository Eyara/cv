document.addEventListener('DOMContentLoaded', function() {

	var progress = false;

	$(function() {
		smoothScroll(1000);
	});

	// smoothScroll function is applied from the document ready function
	function smoothScroll (duration) {
		$('a[href^="#"]').on('click', function(event) {

			var target = $( $(this).attr('href') );

			if (target.length) {
				event.preventDefault();
				if (progress === true) { return; }
				progress = true;
				$('html, body').animate({
					scrollTop: target.offset().top
				}, duration, function() {
					progress = false;
				});
			}
		});
	};

	var $window = $(window);
	var links = ['home', 'realisations', 'competences', 'projets', 'contact'];

	$window.on("scroll",function(e){
		links.forEach(function (item, index, array) {
			if (index < links.length - 1) {
				if ($window.scrollTop() >= $('#' + item).offset().top - 20 && $window.scrollTop() < $('#' + links[index + 1]).offset().top - 20) {
					$("#nav-wrap").find("a").removeClass('current');
					$('a[href=\'#' + item + '\']').addClass('current');
				}
			} else if (index === links.length - 1) {
				if ($window.scrollTop() >= $('#' + item).offset().top - 20) {
					$("#nav-wrap").find("a").removeClass('current');
					$('a[href=\'#' + item + '\']').addClass('current');
				}
			}
		});
	});
});