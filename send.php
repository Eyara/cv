<?php
session_start();

$numbers = explode("FA542B5C", $_SESSION["captcha"]);

$resultExpected = intval($numbers[0]) + intval($numbers[1]);

$requiredFields = 
[
	"nom",
	"prenom",
	"mail",
	"tel",
	"captcha",
	"message"
];

foreach ($requiredFields as $value)
{
	if (isset($_POST[$value]) && !empty($_POST[$value]))
	{
		$error[$value] = false;
		if ($value === "nom" || $value === "prenom")
		{
			if (!preg_match("/^[a-záàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ]([a-záàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ]+[\-\ ]?[a-záàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ]+)+[a-záàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ]$/i", $_POST[$value]))
			{
				$error[$value] = true;
			}
		}
		elseif ($value === "mail")
		{
			if (!preg_match("/^[\w\d][\w\d\._\-]+[\w\d]@([\w\d][\w\d\._\-]{0,61}[\w\d]|[\w\d])\.[\w]{1,6}$/i", $_POST[$value]))
			{
				$error[$value] = true;
			}
		}
		elseif ($value === "tel")
		{
			if (!preg_match("/^0[0-9]{9}$|^\+33[0-9]{9}$/", $_POST[$value]))
			{
				$error[$value] = true;
			}
		}
		elseif ($value === "captcha")
		{
			if (!preg_match("/^[0-9]{1,2}$/", $_POST[$value]) || intval($_POST[$value]) !== $resultExpected)
			{
				$error[$value] = true;
			}
		}
	}
	else
	{
		$error[$value] = true;
	}
}

$redirect = false;
foreach ($error as $value) 
{
	if ($value === true)
	{
		$redirect = true;
		break;
	}
}

if ($redirect === true)
{
	$_SESSION["error"] = $error;
	$_SESSION["form"] = $_POST;
	header('Location: index.php#contact');
}
else
{
	$tel = $_POST["tel"];
	$message = $_POST["message"];
	$messageLines = explode("\n", $message);
	foreach ($messageLines as $value) 
	{
		$messageLinesClean[] = htmlspecialchars($value);
	}
	$messageClean = implode("<br>", $messageLinesClean);

	// Mail to thomasgoalec@free.fr
	$senderName = $_POST["prenom"] . " " . $_POST["nom"];
	$senderMail = $_POST["mail"];
	$recieverName = "Thomas Goalec";
	$recieverMail = "thomasgoalec@free.fr";
	require "mail-sample.php";
	mail($recieverMail, $subject, $content, $headers);

	// Mail to sender
	$recieverName = $_POST["prenom"] . " " . $_POST["nom"];
	$recieverMail = $_POST["mail"];
	require "mail-sample-copy.php";
	mail($recieverMail, $subject, $content, $headers);

	$antiCheat = 1;
	require "sended.php";
}