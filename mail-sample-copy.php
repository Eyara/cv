<?php

$subject = '(copie) Contact par CV : ' . $senderName;

$content = '
<html>
<head>
  <title>(copie) Contact par CV : ' . $senderName . '</title>
</head>
<body>
	<p>Thomas Goalec à reçu cet e-mail :</p>
  <p>' . $senderName . ' vous envoie ce message via votre CV en ligne :</p>
  <p>' . $messageClean . '</p>
  <p>Vous pouvez recontacter ' . $senderName . ' au ' . $tel . '.</p>
  <p style="position: absolute; bottom: 0; margin: 10px 0; color: #808080; border-top: 1px solid #808080; border-bottom: 1px solid #808080; padding: 5px 10px;">Ce mail à été vérifié par Rainbow Dash (et en 10 secondes !)</p>
  <img src="http://'.$_SERVER["HTTP_HOST"].'/'.basename(dirname(__file__)).'/images/sealOfProof.png" alt="Seal of proof" style="position: absolute; right: 0; bottom: 0; margin: 10px;" draggable="false">
</body>
</html>
';

$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

$headers .= 'To: ' . $recieverName . ' <' . $recieverMail . '>' . "\r\n";
$headers .= 'From: ' . $senderName . ' <' . $senderMail . '>' . "\r\n";